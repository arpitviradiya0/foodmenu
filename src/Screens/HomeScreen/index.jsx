import { useFocusEffect, useNavigation } from '@react-navigation/core';
import React, { useCallback, useEffect, useState } from 'react';
import { View, Dimensions } from 'react-native';
import * as userActions from '../../redux/actions/userActions';
import firebase from 'firebase';
import { useDispatch, useSelector } from 'react-redux';
import FoodList from '../../Components/FoodList';

const HomeScreen = (props) => {
	const dispatch = useDispatch();
	const navigation = useNavigation();
	const [loading, setLoading] = useState(true);
	const uid = useSelector((state) => state.user.uid);
	const food = useSelector((state) => state.user.food.food);
	console.log(`food`, food);
	const getUser = async () => {
		await firebase
			.firestore()
			.collection('users')
			.doc(uid)
			.get()
			.then((userSnapshot) => {
				if (userSnapshot.exists) {
					//console.log('userSnapshot.data()----', userSnapshot.data());
					dispatch(userActions.setUserData(userSnapshot.data()));
				}
			});
		if (loading) {
			setLoading(false);
		}
	};
	const getFood = async () => {
		await firebase
			.firestore()
			.collection('foodItems')
			.get()
			.then((userSnapshot) => {
				if (userSnapshot.exists) {
					//console.log('userSnapshot.data()----', userSnapshot.data());
					dispatch(userActions.getFoodData(userSnapshot.data()));
				}
			});
		if (loading) {
			setLoading(false);
		}
	};
	useFocusEffect(
		useCallback(() => {
			getUser();
			getFood();
		})
	);
	return (
		<View style={{ flex: 1 }}>
			<FoodList />
		</View>
	);
};
export default HomeScreen;
