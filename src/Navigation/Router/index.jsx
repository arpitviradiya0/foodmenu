import React from 'react';
import { View, Text, StyleSheet, Button, Pressable } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../../Screens/HomeScreen';
import WishList from '../../Screens/WishList';
import { Entypo } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/core';

const Stack = createStackNavigator();

const Router = (props) => {
	const navigation = useNavigation();

	return (
		<Stack.Navigator>
			<Stack.Screen
				name={'Food Menu'}
				component={HomeScreen}
				options={{
					headerShown: true,
					headerTitleAlign: 'center',
					headerLeft: () => (
						<Pressable onPress={() => navigation.openDrawer()}>
							<Entypo
								name={'menu'}
								size={30}
								color='black'
								style={{ marginLeft: 15 }}
							/>
						</Pressable>
					),
					headerRight: () => (
						<Pressable
							onPress={() => navigation.navigate('Wishlist')}
						>
							<Entypo
								name={'heart'}
								size={30}
								color='black'
								style={{ marginRight: 15 }}
							/>
						</Pressable>
					),
				}}
			/>
			<Stack.Screen
				name={'Wishlist'}
				component={WishList}
				options={{ headerShown: false }}
			/>
		</Stack.Navigator>
	);
};
export default Router;

const styles = StyleSheet.create({});
