import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
	item: {
		backgroundColor: 'blue',
		alignItems: 'center',
		justifyContent: 'center',
		height: 150,
		flex: 1,
		margin: 1,
	},
	itemText: {
		fontSize: 30,
		color: 'red',
	},
});

export default styles;
