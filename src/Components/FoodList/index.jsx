import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import styles from './styles';

const dataList = [
	{ key: 1 },
	{ key: 2 },
	{ key: 3 },
	{ key: 4 },
	{ key: 5 },
	{ key: 6 },
	{ key: 7 },
	{ key: 8 },
	{ key: 9 },
	{ key: 10 },
	{ key: 12 },
	{ key: 13 },
	{ key: 14 },
	{ key: 15 },
	{ key: 16 },
	{ key: 17 },
];

const FoodList = (props) => {
	const renderList = ({ item }) => {
		return (
			<View style={styles.item}>
				<Text style={styles.itemText}>{item.key}</Text>
			</View>
		);
	};

	return (
		<View>
			<FlatList
				numColumns='2'
				data={dataList}
				keyExtractor={(item) => item.key}
				renderItem={renderList}
			/>
		</View>
	);
};
export default FoodList;
